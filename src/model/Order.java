package model;

import com.sun.org.apache.xpath.internal.operations.Bool;
import sun.applet.Main;
import view.MainFrame;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private Integer id;
    private String street;
    private String streetNumber;
    private String town;
    private Boolean building;
    private Integer floor;
    private Integer apartmentNumber;
    private String customerName;
    private String customerPhoneNumber;
    private String receiptEmployee;
    private LocalDate receiptDate;
    private Boolean furniture;
    private String state; // { Kreirana / Pokupljena / Spremna / Na vraćanju / Vraćena / Završena}
    private Double quadrature;
    private Double pricePerQ;
    private Boolean discount;
    private Double discountPercentage;
    private Integer carpetNumber;
    private Double transportCost;
    private Double price;
    private String retrievalEmployee;
    private LocalDate retrievalDate;


    public Order() {
    }

    public Order(Integer id, String street, String streetNumber,
                 String town, Boolean inBuilding, Integer floor,
                 Integer apartmentNumber, String customerName,
                 String customerPhoneNumber, String receiptEmployee,
                 LocalDate receiptDate, Boolean furniture, String status) {
        this.id = id;
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
        this.building = inBuilding;
        this.floor = floor;
        this.apartmentNumber = apartmentNumber;
        this.customerName = customerName;
        this.customerPhoneNumber = customerPhoneNumber;
        this.receiptEmployee = receiptEmployee;
        this.receiptDate = receiptDate;
        this.furniture = furniture;
        this.state = status;
    }

    public Order(Integer id, String street, String streetNumber, String town, Boolean building, Integer floor, Integer apartmentNumber, String customerName, String customerPhoneNumber, String receiptEmployee, LocalDate receiptDate, Boolean furniture, String state, Double quadrature, Double pricePerQ, Boolean discount, Double discountPercentage, Integer carpetNumber, Double price, String retrievalEmployee, LocalDate retrievalDate, Double transportCost) {
        this.id = id;
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
        this.building = building;
        this.floor = floor;
        this.apartmentNumber = apartmentNumber;
        this.customerName = customerName;
        this.customerPhoneNumber = customerPhoneNumber;
        this.receiptEmployee = receiptEmployee;
        this.receiptDate = receiptDate;
        this.furniture = furniture;
        this.state = state;
        this.quadrature = quadrature;
        this.pricePerQ = pricePerQ;
        this.discount = discount;
        this.discountPercentage = discountPercentage;
        this.carpetNumber = carpetNumber;
        this.price = price;
        this.retrievalEmployee = retrievalEmployee;
        this.retrievalDate = retrievalDate;
        this.transportCost = transportCost;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getBuilding() {
        return building;
    }

    public void setBuilding(Boolean building) {
        this.building = building;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(Integer apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getReceiptEmployee() {
        return receiptEmployee;
    }

    public void setReceiptEmployee(String receiptEmployee) {
        this.receiptEmployee = receiptEmployee;
    }

    public LocalDate getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(LocalDate receiptDate) {
        this.receiptDate = receiptDate;
    }

    public Boolean getFurniture() {
        return furniture;
    }

    public void setFurniture(Boolean furniture) {
        this.furniture = furniture;
    }

    public String getState() {
        return state;
    }

    public void setState(String status) {
        this.state = status;
    }

    public Double getQuadrature() {
        return quadrature;
    }

    public void setQuadrature(Double quadrature) {
        this.quadrature = quadrature;
    }

    public Double getPricePerQ() {
        return pricePerQ;
    }

    public void setPricePerQ(Double pricePerQ) {
        this.pricePerQ = pricePerQ;
    }

    public Boolean getDiscount() {
        return discount;
    }

    public void setDiscount(Boolean discount) {
        this.discount = discount;
    }

    public Double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public Integer getCarpetNumber() {
        return carpetNumber;
    }

    public void setCarpetNumber(Integer carpetNumber) {
        this.carpetNumber = carpetNumber;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getRetrievalEmployee() {
        return retrievalEmployee;
    }

    public void setRetrievalEmployee(String retrievalEmployee) {
        this.retrievalEmployee = retrievalEmployee;
    }

    public LocalDate getRetrievalDate() {
        return retrievalDate;
    }

    public void setRetrievalDate(LocalDate retrievalDate) {
        this.retrievalDate = retrievalDate;
    }

    public Double getTransportCost() {
        return transportCost;
    }

    public void setTransportCost(Double transportCost) {
        this.transportCost = transportCost;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                ", town='" + town + '\'' +
                ", building=" + building +
                ", floor=" + floor +
                ", apartmentNumber=" + apartmentNumber +
                ", customerName='" + customerName + '\'' +
                ", customerPhoneNumber='" + customerPhoneNumber + '\'' +
                ", receiptEmployee='" + receiptEmployee + '\'' +
                ", receiptDate=" + receiptDate +
                ", furniture=" + furniture +
                ", state='" + state + '\'' +
                ", quadrature=" + quadrature +
                ", pricePerQ=" + pricePerQ +
                ", discount=" + discount +
                ", discountPercentage=" + discountPercentage +
                ", carpetNumber=" + carpetNumber +
                ", transportCost=" + transportCost +
                ", price=" + price +
                ", retrievalEmployee='" + retrievalEmployee + '\'' +
                ", retrievalDate=" + retrievalDate +
                '}';
    }

    @SuppressWarnings("Duplicates")
    public static boolean createNewOrder(Order order) {
        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            int bValue = order.building? 1 : 0;
            int fValue = order.furniture? 1 : 0;
            String query =
                    "INSERT INTO narudzba (id, street, street_number, town, building, floor, apartment_number, customer_name, customer_phone_number, receipt_employee, receipt_date, furniture, state)" +
                    "values('" + order.id + "','" + order.street + "','" + order.streetNumber + "','" +
                            order.town + "','" + bValue + "','" + order.floor + "','" + order.apartmentNumber + "','" +
                            order.customerName + "','" + order.customerPhoneNumber + "','" + order.receiptEmployee + "','" +
                            order.receiptDate + "','" + fValue + "','" + order.state + "')";

            Statement sta = connection.createStatement();
            int x = sta.executeUpdate(query);
            connection.close();
            if (x == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }

    @SuppressWarnings("Duplicates")
    public static boolean defineCarpetSettings(Integer id, Double quadrature, Integer carpetNumber,
                                               Double pricePerQ, Boolean discountFlag,
                                               Double discountPercentage, Double price,
                                               Double transportCost) {
        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            // If we are defining carpet attributes -> state of order is : Pokupljena
            int dValue = discountFlag? 1 : 0;
            String queryUpdate =
                    "UPDATE narudzba SET quadrature = '" + quadrature + "', state = 'Pokupljena', carpet_number = '" + carpetNumber + "', price_per_q = '" + pricePerQ  + "', discount = '" + dValue + "', discount_percentage = '" + discountPercentage + "', price = '" + price + "', transport_cost = '" + transportCost + "' WHERE id = '" + id +"'";

            Statement sta = connection.createStatement();
            int x = sta.executeUpdate(queryUpdate);
            connection.close();
            if (x == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }

    @SuppressWarnings("Duplicates")
    public static boolean changeStatus(Integer id, String newState) {
        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            String queryUpdate = "";

            if (newState.equals("Na vraćanju")) {
                LocalDate date = LocalDate.now();
                queryUpdate = "UPDATE narudzba SET state = '" + newState + "', retrieval_date = '" + date + "' WHERE id = '" + id +"'";
            } else {
                queryUpdate = "UPDATE narudzba SET state = '" + newState + "' WHERE id = '" + id +"'";
            }


            Statement sta = connection.createStatement();
            int x = sta.executeUpdate(queryUpdate);
            connection.close();
            if (x == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }

    @SuppressWarnings("Duplicates")
    public static boolean carpetsOnTransport(Integer id, String retrievalEmployee) {
        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            // If transport is clicked -> state of order is : Na vraćanju
            LocalDate date = LocalDate.now();
            String queryUpdate = "UPDATE narudzba SET state = 'Na vraćanju', retrieval_employee = '" + retrievalEmployee + "', retrieval_date = '" + date  + "' WHERE id = '" + id +"'";

            Statement sta = connection.createStatement();
            int x = sta.executeUpdate(queryUpdate);
            connection.close();
            if (x == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }

    @SuppressWarnings("Duplicates")
    public static String getCurrentStatus(Integer id) {
        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            String querySelectId =
                    "SELECT state FROM narudzba WHERE id = '" + id + "'";

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(querySelectId);
            String state = "/";
            while (rs.next())
            {
                state = rs.getString("state");
                break;
            }
            rs.close();
            connection.close();
            return state;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return "/";
    }

    @SuppressWarnings("Duplicates")
    public static List<Order> getOrders() {

        List<Order> orders = new ArrayList<>();

        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            String querySelectId =
                    "SELECT * FROM narudzba";

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(querySelectId);
            while (rs.next())
            {
                Integer id = rs.getInt("id");
                String state = rs.getString("state");
                String street = rs.getString("street");
                String streetNum = rs.getString("street_number");
                String town = rs.getString("town");
                Boolean building = rs.getBoolean("building");
                Integer floor = rs.getInt("floor");
                Integer appNum = rs.getInt("apartment_number");
                String customer_name = rs.getString("customer_name");
                String customer_phone = rs.getString("customer_phone_number");
                String receiptEmployee = rs.getString("receipt_employee");
                Boolean furniture = rs.getBoolean("furniture");
                Double quadrature = rs.getDouble("quadrature");
                Double pricePQ = rs.getDouble("price_per_q");
                Boolean discount = rs.getBoolean("discount");
                Double discountPercentage = rs.getDouble("discount_percentage");
                Integer carpetNum = rs.getInt("carpet_number");
                Double price = rs.getDouble("price");
                Double transportCost = rs.getDouble("transport_cost");
                String retrievalEmployee = rs.getString("retrieval_employee");
                LocalDate receiptDate = null;
                LocalDate retrievalDate = null;
                try {
                    receiptDate = rs.getDate("receipt_date").toLocalDate();
                    retrievalDate = rs.getDate("retrieval_date").toLocalDate();
                } catch (Exception e){

                }

                Order order = new Order(id, street, streetNum, town, building, floor, appNum,
                        customer_name, customer_phone, receiptEmployee, receiptDate, furniture,
                        state, quadrature, pricePQ, discount, discountPercentage, carpetNum,
                        price, retrievalEmployee, retrievalDate, transportCost);
                orders.add(order);
            }
            rs.close();
            connection.close();
            return orders;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return orders;
    }

    @SuppressWarnings("Duplicates")
    public static Order getOrder(Integer id) {

        Order order = null;

        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            String querySelectAll =
                    "SELECT * FROM narudzba WHERE id = '" + id +"'";;

            Statement st1 = connection.createStatement();

            ResultSet rs1 = st1.executeQuery(querySelectAll);
            while (rs1.next())
            {
                String state = rs1.getString("state");
                String street = rs1.getString("street");
                String streetNum = rs1.getString("street_number");
                String town = rs1.getString("town");
                Boolean building = rs1.getBoolean("building");
                Integer floor = rs1.getInt("floor");
                Integer appNum = rs1.getInt("apartment_number");
                String customer_name = rs1.getString("customer_name");
                String customer_phone = rs1.getString("customer_phone_number");
                String receiptEmployee = rs1.getString("receipt_employee");
                Boolean furniture = rs1.getBoolean("furniture");
                Double quadrature = rs1.getDouble("quadrature");
                Double pricePQ = rs1.getDouble("price_per_q");
                Boolean discount = rs1.getBoolean("discount");
                Double discountPercentage = rs1.getDouble("discount_percentage");
                Integer carpetNum = rs1.getInt("carpet_number");
                Double price = rs1.getDouble("price");
                Double transportCost = rs1.getDouble("transport_cost");
                String retrievalEmployee = rs1.getString("retrieval_employee");
                LocalDate receiptDate = null;
                LocalDate retrievalDate = null;
                try {
                    receiptDate = rs1.getDate("receipt_date").toLocalDate();
                    retrievalDate = rs1.getDate("retrieval_date").toLocalDate();
                } catch (Exception e){

                }

                order = new Order(id, street, streetNum, town, building, floor, appNum,
                        customer_name, customer_phone, receiptEmployee, receiptDate, furniture,
                        state, quadrature, pricePQ, discount, discountPercentage, carpetNum,
                        price, retrievalEmployee, retrievalDate, transportCost);
                break;
            }
            rs1.close();
            connection.close();
            return order;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return order;
    }

    @SuppressWarnings("Duplicates")
    public static Integer getIDForNewOrder() {
        Integer id = 0;

        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);


            String querySelectId =
                    "SELECT id from narudzba ORDER BY id DESC LIMIT 1";

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(querySelectId);
            while (rs.next())
            {
                id = rs.getInt("id");
                break;
            }
            rs.close();
            connection.close();
            id += 1;

            return id;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return id;
    }

    @SuppressWarnings("Duplicates")
    public static Integer getSameAddressNum(Order order) {

        Integer num = 0;

        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            int b = order.getBuilding()? 1 : 0;
            String query =
                    "SELECT COUNT(*) AS NUM FROM narudzba WHERE street = '" + order.getStreet() + "' AND street_number = '" + order.getStreetNumber() + "' AND town = '" + order.getTown() + "' AND building = '" + b + "' AND floor = '" + order.getFloor() + "' AND apartment_number = '" + order.getApartmentNumber() + "'" ;

            Statement st1 = connection.createStatement();
            ResultSet rs = st1.executeQuery(query);
            while (rs.next())
            {
                num = rs.getInt("num");
                break;
            }
            rs.close();
            connection.close();
            num -=1;
            return num;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return num;
    }

    @SuppressWarnings("Duplicates")
    public static boolean deleteOrder(Integer id) {
        try {
            Connection connection = DriverManager.getConnection(MainFrame.hostDB, MainFrame.uNameDB, MainFrame.pwdDB);

            String queryUpdate = "DELETE FROM narudzba WHERE id = '" + id +"'";

            Statement sta = connection.createStatement();
            int x = sta.executeUpdate(queryUpdate);
            connection.close();
            if (x == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }
}
