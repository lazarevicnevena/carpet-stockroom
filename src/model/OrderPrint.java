package model;

import view.MainFrame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderPrint {
    private Integer id;
    private String street;
    private String streetNumber;
    private Boolean building;
    private Integer floor;
    private Integer apartmentNumber;
    private String customerPhoneNumber;
    private Double quadrature;
    private Double price;


    public OrderPrint() {
    }

    public OrderPrint(Integer id, String street, String streetNumber, Boolean building, Integer floor, Integer apartmentNumber, String customerPhoneNumber, Double quadrature, Double price) {
        this.id = id;
        this.street = street;
        this.streetNumber = streetNumber;
        this.building = building;
        this.floor = floor;
        this.apartmentNumber = apartmentNumber;
        this.customerPhoneNumber = customerPhoneNumber;
        this.quadrature = quadrature;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Boolean getBuilding() {
        return building;
    }

    public void setBuilding(Boolean building) {
        this.building = building;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(Integer apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public Double getQuadrature() {
        return quadrature;
    }

    public void setQuadrature(Double quadrature) {
        this.quadrature = quadrature;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderPrint{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                ", building=" + building +
                ", floor=" + floor +
                ", apartmentNumber=" + apartmentNumber +
                ", customerPhoneNumber='" + customerPhoneNumber + '\'' +
                ", quadrature=" + quadrature +
                ", price=" + price +
                '}';
    }
}
