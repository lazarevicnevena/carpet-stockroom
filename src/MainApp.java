import view.MF;
import view.MainFrame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.File;

public class MainApp {

    public static void main(String[] args) {

        JFrame mf = MF.getInstance().getFrame();
        mf.setVisible(true);
    }

}
