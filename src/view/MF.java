package view;

import model.OrderPrint;

import javax.swing.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MF {
    private JFrame frame;
    public static MF instance;
    private List<OrderPrint> orderPrints = new ArrayList<>();
    private String date;

    public MF() {
        frame = new JFrame("Tepih Servis GORAN");
        frame.setContentPane(MainFrame.getInstance().getPanel());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        LocalDate d = LocalDate.now();
        String date = "";
        String day = d.getDayOfWeek().toString();
        if (day.equals("MONDAY")) {
            date += "PONEDELJAK  ";
        } else if (day.equals("TUESDAY")) {
            date += "UTORAK  ";
        } else if (day.equals("WEDNESDAY")) {
            date += "SREDA  ";
        } else if (day.equals("THURSDAY")) {
            date += "ČETVRTAK  ";
        } else if (day.equals("FRIDAY")) {
            date += "PETAK  ";
        } else if (day.equals("SATURDAY")) {
            date += "SUBOTA  ";
        } else if (day.equals("SUNDAY")) {
            date += "NEDELJA  ";
        }
        date += d.getDayOfMonth() + "." + d.getMonthValue() + "." + d.getYear();
        this.date = date;
        /*
        try {
            frame.setIconImage(ImageIO.read(new File("src/resources.images/logo-resized.png")));
        }catch (Exception e) {}
         */

        frame.pack();


        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        }catch (Exception e) {}
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    public JFrame getFrame() {
        return frame;
    }

    public static MF getInstance() {
        if (instance == null) {
            instance =  new MF();
        }
        return instance;
    }

    public List<OrderPrint> getOrderPrints() {
        return orderPrints;
    }

    public String getDate() {
        return date;
    }
}
