package view;

import model.Order;
import model.OrderPrint;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class StockRoomPanel {
    private JPanel panel;
    private JPanel panelLeft;
    private JPanel panelRight;
    private JComboBox stateInput;
    private JTextField customerInput;
    private JTextField addressInput;
    private JTextField orderInput;
    private JButton deleteButton;
    private JButton addButton;

    private List<Order> allOrders = new ArrayList<>();
    private String[] columnNames = {
                    "Šifra", "Ulica", "Grad", "Status", "Klijent",
                    "Broj telefona", "Datum prijema", "Radnik na prijemu",
                    "Kvadratura", "Broj tepiha", "Cena",
                    "Radnik na povratu", "Datum povrata"};

    private Object[] options = {"DA", "NE"};

    private JTable ordersList;

    public StockRoomPanel() {
        populateTable();
        stateInput.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                // Because one event is also deselected
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String status = String.valueOf(stateInput.getSelectedItem());
                    if (status.equals("Status")) {
                        generateTable(allOrders);
                    } else {
                        List<Order> orders = getOrdersByStatus(status);
                        generateTable(orders);
                    }
                }
            }
        });
        customerInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                search();
            }
        });

        orderInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                search();
            }
        });

        addressInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                search();
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int selectedRowIndex = ordersList.getSelectedRow();
                    Integer id = Integer.parseInt(ordersList.getValueAt(selectedRowIndex, 0).toString());
                    int dialogResult = JOptionPane.showOptionDialog(null,
                            "Da li ste sigurni da želite izbrisati narudžbu?", "Oprez",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null, options, options[1]);
                    if (dialogResult == JOptionPane.NO_OPTION) {
                        return;
                    }
                    Boolean result = Order.deleteOrder(id);
                    if (result) {
                        allOrders.clear();
                        allOrders = Order.getOrders();
                        generateTable(allOrders);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Nije moguće izbrisati narudžbu iz skladišta.", "Greška", JOptionPane.PLAIN_MESSAGE);
                }
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRowIndex = ordersList.getSelectedRow();
                Integer id = Integer.parseInt(ordersList.getValueAt(selectedRowIndex, 0).toString());
                Order order = Order.getOrder(id);
                if (order != null) {
                    OrderPrint orderPrint = new OrderPrint(id, order.getStreet(), order.getStreetNumber(),
                            order.getBuilding(), order.getFloor(), order.getApartmentNumber(), order.getCustomerPhoneNumber(),
                            order.getQuadrature(), order.getPrice());

                    Boolean flag = true;
                    for (OrderPrint op: MF.getInstance().getOrderPrints()) {
                        if (op.getId().equals(id)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        MF.getInstance().getOrderPrints().add(orderPrint);
                        JOptionPane.showMessageDialog(null, "Narudžba uspešno dodata u tabelu za štampanje.", "Obaveštenje", JOptionPane.PLAIN_MESSAGE);

                    } else {
                        JOptionPane.showMessageDialog(null, "Narudžba je već dodata u tabelu za štampanje.", "Greška", JOptionPane.PLAIN_MESSAGE);

                    }

                }
            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }

    public void populateTable(){
        List<Order> list = Order.getOrders();
        allOrders = list;
        generateTable(list);
    }

    public List<Order> getOrdersByStatus(String status) {
        List<Order> newOrders = new ArrayList<>();
        for (Order order: allOrders) {
            if (order.getState().equals(status)) {
                newOrders.add(order);
            }
        }
        return newOrders;
    }

    public List<Order> getOrdersByCriteria(List<Order> orders) {
        String addressValue = addressInput.getText().toLowerCase();
        String orderInputV = orderInput.getText().toLowerCase();
        String customerInputV = customerInput.getText().toLowerCase();
        List<Order> newOrders = new ArrayList<>();
        for (Order order: orders) {
            Boolean flag = true;
            if (!customerInputV.equals("")) {
                if (!order.getCustomerName().toLowerCase().contains(customerInputV) &&
                        !order.getCustomerPhoneNumber().toLowerCase().contains(customerInputV)) {
                    flag = false;
                }
            }
            if (!addressValue.equals("")) {
                if (!order.getStreet().toLowerCase().contains(addressValue) &&
                        !order.getStreetNumber().toLowerCase().contains(addressValue) &&
                        !order.getTown().toLowerCase().contains(addressValue)) {
                    flag = false;
                }
            }
            if (!orderInputV.equals("")) {
                if (!order.getId().toString().toLowerCase().contains(orderInputV)) {
                    flag = false;
                }
            }
            if (flag) {
                newOrders.add(order);
            }
        }
        return newOrders;
    }

    public void generateTable(List<Order> list) {
        panelRight.removeAll();
        panelRight.revalidate();

        Object[][] arr = new Object[list.size()][7];

        for (Order order: list) {
            String date = "-";
            String date1 = "-";
            try {
                date1 = order.getReceiptDate().toString();
                date = order.getRetrievalDate().toString();
            }catch (Exception e) {}

            String address = order.getStreet() + " " + order.getStreetNumber();
            if (order.getBuilding()) {
                address += " (" + order.getApartmentNumber() + "/" + order.getFloor() + ")";
            }
            String[] row = {order.getId().toString(), address,
                    order.getTown(), order.getState(), order.getCustomerName(),
                    order.getCustomerPhoneNumber(), date1,
                    order.getReceiptEmployee(),
                    order.getQuadrature().toString(), order.getCarpetNumber().toString(),
                    order.getPrice().toString() + " DIN", order.getRetrievalEmployee(),
                    date};
            int index = list.indexOf(order);
            arr[index] = row;
        }

        ordersList = new JTable(arr, columnNames);
        ordersList.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ordersList.setAutoCreateRowSorter(true);
        ordersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ordersList.setDefaultEditor(Object.class, null);

        ListSelectionModel selectionModel = ordersList.getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                deleteButton.setVisible(true);
                addButton.setVisible(true);
            }
        });

        JScrollPane sp = new JScrollPane(ordersList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        sp.setBorder(BorderFactory.createEmptyBorder());
        panelRight.add(sp, BorderLayout.CENTER);
        panelRight.updateUI();
    }

    public void search(){
        String status = String.valueOf(stateInput.getSelectedItem());
        if (status.equals("Status")) {
            generateTable(getOrdersByCriteria(allOrders));
        } else {
            List<Order> orders = getOrdersByStatus(status);
            generateTable(getOrdersByCriteria(orders));
        }
    }
}
