package view;

import controller.DateLabelFormatter;
import model.Order;
import org.jdatepicker.JDatePicker;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class OrderPanel {
    private JTextField streetInput;
    private JPanel panel;
    private JTextField townInput;
    private JSpinner floorInput;
    private JTextField customerNameInput;
    private JTextField customerPhoneInput;
    private JCheckBox flagOption;
    private JTextField employeeInput;
    private JButton cancelButton;
    private JButton createOrderButton;
    private JTextField idInput;
    private JTextField dateInput;
    private JTextField streetNumInput;
    private JSpinner appNumInput;
    private JCheckBox buildingCheckBox;
    private JLabel floorLabel;
    private JLabel appLabel;
    private JPanel panelDate;

    private JDatePickerImpl datePicker;

    public OrderPanel() {

        initializeSpinners();
        initializeID();

        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                MainFrame.getInstance().emptyPanelInput();
            }
        });
        buildingCheckBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (buildingCheckBox.isSelected()){
                    appLabel.setVisible(true);
                    appNumInput.setVisible(true);
                    floorLabel.setVisible(true);
                    floorInput.setVisible(true);
                } else {
                    appLabel.setVisible(false);
                    appNumInput.setVisible(false);
                    floorLabel.setVisible(false);
                    floorInput.setVisible(false);
                }
            }
        });
        createOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }

    public void initializeSpinners() {
        SpinnerModel spinnerModelF = new SpinnerNumberModel(0, 0, 5000, 1);
        floorInput.setModel(spinnerModelF);
        SpinnerModel spinnerModelA = new SpinnerNumberModel(0, 0, 5000, 1);
        appNumInput.setModel(spinnerModelA);

        UtilDateModel dateModel = new UtilDateModel();
        LocalDate date = LocalDate.now();
        dateModel.setDate(date.getYear(), date.getMonthValue()-1, date.getDayOfMonth());
        dateModel.setSelected(true);
        Properties properties = new Properties();
        properties.put("text.today", "Danas");
        properties.put("text.month", "Mesec");
        properties.put("text.year", "Godina");
        JDatePanelImpl datePanel = new JDatePanelImpl(dateModel, properties);
        datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        panelDate.add(datePicker);
    }

    public void initializeID() {
        Integer id = Order.getIDForNewOrder();
        idInput.setText(id.toString());
    }

    public void saveData(){
        Integer idValue = Integer.parseInt(idInput.getText());
        String employeeInp = employeeInput.getText();
        String custName = customerNameInput.getText();
        String custPhone = customerPhoneInput.getText();
        String street = streetInput.getText();
        String town = townInput.getText();
        String sNum = streetNumInput.getText();

        if (idValue.equals("") || employeeInp.equals("") || custName.equals("") ||
                custPhone.equals("") || street.equals("") || town.equals("") || sNum.equals("") || !datePicker.getModel().isSelected()){
            //showMessageDialog("Oprez", "Neki podaci nisu definisani, popunite polja i pokušajte ponovo.");
            //return;
        }
        Integer appNum = 0;
        Integer floor = 0;
        Boolean isBuilding = buildingCheckBox.isSelected();
        if (isBuilding){
            appNum = Integer.parseInt(appNumInput.getValue().toString());
            floor = Integer.parseInt(floorInput.getValue().toString());
        }

        Date selectedDate = (Date) datePicker.getModel().getValue();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(selectedDate);

        LocalDate receiptDate = LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.DAY_OF_MONTH));

        Order order = new Order(idValue, street, sNum, town, isBuilding, floor, appNum, custName, custPhone,
                employeeInp, receiptDate, flagOption.isSelected(), "Kreirana");

        Boolean result = Order.createNewOrder(order);
        if (result) {
            showMessageDialog("Obaveštenje", "Naružba uspešno kreirana.");
            MainFrame.getInstance().emptyPanelInput();
        } else {
            showMessageDialog("Greška", "Došlo je do greške prilikom čuvanja podataka. Pokušajte ponovo.");
        }

    }

    public void showMessageDialog(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.PLAIN_MESSAGE);
    }
}
