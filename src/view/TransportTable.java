package view;

import model.Order;
import model.OrderPrint;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TransportTable {
    private JPanel panelTable;
    private JPanel panelLeft;
    private JButton deleteButton;
    private JButton addButton;
    private JPanel panel;
    private JButton resetTableButton;

    private JTable ordersList;

    private String[] columnNames = {
            "Redni Br.", "Šifra", "Adresa",
            "Broj telefona",
            "Kvadratura", "Cena", "Plaćeno"};

    int[] columnsWidth = {
            65, 50, 175, 100, 70, 65, 75
    };

    public TransportTable() {
        generateTable(MF.getInstance().getOrderPrints());
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRowIndex = ordersList.getSelectedRow();
                Integer id = Integer.parseInt(ordersList.getValueAt(selectedRowIndex, 1).toString());
                for (OrderPrint op: MF.getInstance().getOrderPrints()) {
                    if (op.getId().equals(id)) {
                        MF.getInstance().getOrderPrints().remove(op);
                        break;
                    }
                }
                generateTable(MF.getInstance().getOrderPrints());
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printTablePanel();
            }
        });
        resetTableButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MF.getInstance().getOrderPrints().clear();
                generateTable(new ArrayList<>());
            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }

    @SuppressWarnings("Duplicates")
    public void generateTable(List<OrderPrint> list) {
        panelTable.removeAll();
        panelTable.revalidate();
        panelTable.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), MF.getInstance().getDate(), TitledBorder.CENTER, TitledBorder.TOP));
        //panelTable.setBackground(Color.WHITE);

        Object[][] arr = new Object[list.size()][7];

        for (OrderPrint order: list) {

            String address = order.getStreet() + " " + order.getStreetNumber();
            if (order.getBuilding()) {
                address += " (" + order.getApartmentNumber() + "/" + order.getFloor() + ")";
            }

            String[] row = {"", order.getId().toString(), address,
                    order.getCustomerPhoneNumber(), order.getQuadrature().toString(),
                    order.getPrice().toString(), ""};
            int index = list.indexOf(order);
            arr[index] = row;
        }

        ordersList = new JTable(arr, columnNames);
        ordersList.setGridColor(Color.BLACK);
        ordersList.setBackground(Color.WHITE);
        ordersList.getTableHeader().setBackground(Color.WHITE);
        ordersList.setOpaque(true);
        int i = 0;
        for (int width : columnsWidth) {
            TableColumn column = ordersList.getColumnModel().getColumn(i++);
            column.setMinWidth(width);
            column.setMaxWidth(width);
            column.setPreferredWidth(width);
        }

        ordersList.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ordersList.setAutoCreateRowSorter(true);
        ordersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ordersList.setDefaultEditor(Object.class, null);
        ordersList.setShowHorizontalLines(true);
        ordersList.setShowVerticalLines(true);

        ListSelectionModel selectionModel = ordersList.getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                deleteButton.setVisible(true);
            }
        });
        JScrollPane sp = new JScrollPane(ordersList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        sp.setBorder(BorderFactory.createEmptyBorder());
        sp.getViewport().setBackground(Color.WHITE);
        panelTable.add(sp, BorderLayout.CENTER);
        panelTable.updateUI();
    }

    public void printTablePanel(){
        try {
            Toolkit tkp = panelTable.getToolkit();
            PrintJob pjp = tkp.getPrintJob(MF.getInstance().getFrame(), null, null);
            Graphics g = pjp.getGraphics();
            panelTable.printAll(g);
            g.dispose();
            pjp.end();
            JOptionPane.showMessageDialog(null, "Štampanje tabele završeno.", "Obaveštenje", JOptionPane.PLAIN_MESSAGE);
            MainFrame.getInstance().emptyPanelInput();
        } catch (Exception pE) {
            pE.printStackTrace();
        }
    }
}
