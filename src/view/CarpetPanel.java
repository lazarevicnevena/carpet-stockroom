package view;

import model.Order;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;

public class CarpetPanel {
    private JPanel panel;
    private JSpinner quadratureInput;
    private JSpinner discountInput;
    private JButton cancelButton;
    private JButton saveButton;
    private JTextField priceFinal;
    private JTextField idInput;
    private JSpinner numberInput;
    private JSpinner pricePerQInput;
    private JCheckBox discountFlag;
    private JLabel discountLabel;
    private JSpinner transportCostInput;

    private final Integer initialPricePerQ = 150;
    private final Integer initialDiscount = 10;
    private final Integer minimumSameAddress = 5;

    public CarpetPanel() {
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                MainFrame.getInstance().emptyPanelInput();
            }
        });

        initializeSpinners();


        discountFlag.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (discountFlag.isSelected()) {
                    discountLabel.setVisible(true);
                    discountInput.setVisible(true);
                } else {
                    discountLabel.setVisible(false);
                    discountInput.setVisible(false);
                }
                calculatePrice();
            }
        });

        quadratureInput.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                calculatePrice();
            }
        });
        pricePerQInput.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                calculatePrice();
            }
        });
        discountInput.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                calculatePrice();
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });
        transportCostInput.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                calculatePrice();
            }
        });

        idInput.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                if (!idInput.getText().equals("")) {
                    try {
                        Integer id = Integer.parseInt(idInput.getText());
                        Order order = Order.getOrder(id);
                        Integer num = Order.getSameAddressNum(order);
                        if (num >= minimumSameAddress) {
                            showMessageDialog("Obaveštenje", "Klijent na ovoj adresi je minimalno pet puta koristio usluge servisa. Razmislite o popustu!");
                        }
                    } catch (Exception ex) { }
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }

    public void initializeSpinners() {
        SpinnerModel spinnerModelQ = new SpinnerNumberModel(0, 0, 5000, 5);
        quadratureInput.setModel(spinnerModelQ);
        SpinnerModel spinnerModelP = new SpinnerNumberModel((int)initialPricePerQ, 1, 5000, 50);
        pricePerQInput.setModel(spinnerModelP);
        SpinnerModel spinnerModelQuantity = new SpinnerNumberModel(1, 1,5000, 1);
        numberInput.setModel(spinnerModelQuantity);
        SpinnerModel spinnerModelD = new SpinnerNumberModel((int)initialDiscount, 1, 5000, 5);
        discountInput.setModel(spinnerModelD);
        SpinnerModel spinnerModelT = new SpinnerNumberModel((int)0, 0, 5000, 100);
        transportCostInput.setModel(spinnerModelT);
    }

    public void calculatePrice() {
        Double quadrValue = Double.parseDouble(quadratureInput.getValue().toString());
        Double pricePerQValue = Double.parseDouble(pricePerQInput.getValue().toString());
        Double regPrice = quadrValue * pricePerQValue;
        Double transportPrice = Double.parseDouble(transportCostInput.getValue().toString());
        if (transportPrice > 0) {
            regPrice += transportPrice;
        }
        if (discountFlag.isSelected()){
            Double disc = Double.parseDouble(discountInput.getValue().toString());
            if (disc > 0) {
                regPrice -= regPrice * disc / 100;
            }
        }

        priceFinal.setText(regPrice.toString() + " DIN");
        priceFinal.setBorder(BorderFactory.createLineBorder(Color.GREEN, 2));
        priceFinal.setFont(priceFinal.getFont().deriveFont(Font.BOLD, 14f));
    }

    public void saveData(){

        String idValue = idInput.getText();
        if (!checkId()) {
            return;
        }

        String textFinal = priceFinal.getText();
        if (textFinal.equals("") || textFinal.equals("0.0 DIN")) {
            showMessageDialog("Obaveštenje", "Ukupna cena mora biti definisana i veća od 0 DIN.");
            return;
        }

        Double qValue = Double.parseDouble(quadratureInput.getValue().toString());
        Double ppqValue = Double.parseDouble(pricePerQInput.getValue().toString());
        Double priceFinal = Double.parseDouble(textFinal.split(" ")[0]);
        Integer carpetNum = Integer.parseInt(numberInput.getValue().toString());
        Double transportCost = Double.parseDouble(transportCostInput.getValue().toString());
        Double percentageV = 0.0;
        if (discountFlag.isSelected()){
            percentageV = Double.parseDouble(discountInput.getValue().toString());
        }

        Integer id = Integer.parseInt(idValue);
        Boolean result = Order.defineCarpetSettings(id, qValue, carpetNum,
                                                    ppqValue, discountFlag.isSelected(),
                                                    percentageV, priceFinal, transportCost);
        if (result) {
            showMessageDialog("Obaveštenje", "Podaci o preuzetoj robi su uspešno sačuvani.");
            MainFrame.getInstance().emptyPanelInput();
        } else {
            showMessageDialog("Greška", "Došlo je do greške prilikom čuvanja izmena. Pokušajte ponovo.");
        }
    }

    public void showMessageDialog(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.PLAIN_MESSAGE);
    }

    @SuppressWarnings("Duplicates")
    public Boolean checkId() {
        if (!idInput.getText().equals("")) {
            try {
                Integer id = Integer.parseInt(idInput.getText());
                Order order = Order.getOrder(id);
                if (order == null) {
                    showMessageDialog("Oprez", "Ne postoji narudžba sa unesenom šifrom!");
                    idInput.setText("");
                    idInput.requestFocus();
                    return false;
                }
            } catch (Exception e) {
                showMessageDialog("Oprez", "Šifra narudžbe može sadržati samo brojeve!");
                idInput.setText("");
                idInput.requestFocus();
                return false;
            }

        } else {
            showMessageDialog("Oprez", "Šifra narudžbe ne može ostati nepopunjena!");
            idInput.setText("");
            idInput.requestFocus();
            return false;
        }
        return true;
    }

}
