package view;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MainFrame {
    private static MainFrame instance;
    private Connection connection;

    private JPanel panel;
    private JSplitPane splitPane;
    private JPanel panelButtons;
    private JPanel panelInput;
    private JPanel panelNew;
    private JPanel panelCarpet;
    private JButton newOrderButton;
    private JButton carpetButton;
    private JButton statusButton;
    private JButton transportButton;
    private JButton stockRoomButton;
    private JPanel panelStatus;
    private JPanel panelTransport;
    private JPanel panelStockRoom;
    private JPanel panelTransportTable;
    private JButton transportTableButton;

    public static final String hostDB = "jdbc:mysql://localhost:3306/tepihcentar?autoReconnect=true&useSSL=false";
    public static final String uNameDB = "root";
    public static final String pwdDB = "nemasifre";

    public MainFrame() {
        newOrderButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setPanelInput(new OrderPanel().getPanel());
            }
        });
        carpetButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setPanelInput(new CarpetPanel().getPanel());
            }
        });
        statusButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setPanelInput(new StatusPanel().getPanel());
            }
        });

        transportButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setPanelInput(new TransportPanel().getPanel());
            }
        });
        stockRoomButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setPanelInput(new StockRoomPanel().getPanel());
            }
        });
        transportTableButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setPanelInput(new TransportTable().getPanel());
            }
        });
    }

    public static MainFrame getInstance(){
        if (instance == null) {
            instance = new MainFrame();
        }
        return instance;
    }

    public JPanel getPanel() {
        return panel;
    }

    public JSplitPane getSplitPane() {
        return splitPane;
    }

    public JPanel getPanelButtons() {
        return panelButtons;
    }

    public JPanel getPanelInput() {
        return panelInput;
    }

    public void setPanelInput(JPanel panelInput) {
        this.panelInput.removeAll();
        this.panelInput.revalidate();
        this.panelInput.add(panelInput);
        this.panelInput.updateUI();
    }

    public void emptyPanelInput() {
        this.panelInput.removeAll();
        this.panelInput.revalidate();
        this.panelInput.updateUI();
    }

    public JPanel getPanelNew() {
        return panelNew;
    }

    public JPanel getPanelCarpet() {
        return panelCarpet;
    }

    public JButton getNewOrderButton() {
        return newOrderButton;
    }

    public JButton getCarpetButton() {
        return carpetButton;
    }

    public JButton getStatusButton() {
        return statusButton;
    }

    public JButton getTransportButton() {
        return transportButton;
    }

    public JButton getStockRoomButton() {
        return stockRoomButton;
    }

    public JPanel getPanelStatus() {
        return panelStatus;
    }

    public JPanel getPanelTransport() {
        return panelTransport;
    }

    public JPanel getPanelStockRoom() {
        return panelStockRoom;
    }

    public Connection getConnection() {
        return connection;
    }

}
