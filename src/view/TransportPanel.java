package view;

import model.Order;

import javax.imageio.ImageIO;
import javax.print.PrintException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterException;
import java.io.File;
import java.text.MessageFormat;

public class TransportPanel {
    private JTextField idInput;
    private JTextField employeeInput;
    private JPanel panel;
    private JButton cancelButton;
    private JButton saveButton;
    private JTextArea receiptInput;
    private JButton prepareButton;
    private JPanel panelRight;
    public JLabel nameInput;
    public JLabel addressInput;
    public JLabel numberPhInput;
    public JLabel quadratureInput;
    public JLabel ppqInput;
    public JLabel transportInput;
    public JLabel discountInput;
    public JLabel carpetNumInput;
    public JLabel priceInput;
    public JLabel receiptDateInput;
    public JLabel retrievalDateInput;
    public JLabel receiptEmployeeInput;
    public JLabel retrievalEmployeeInput;
    private JLabel labelInput;
    private JLabel imageLabel;


    public TransportPanel() {

        initializeLogo();

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainFrame.getInstance().emptyPanelInput();
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printTicketPanel();
            }
        });
        prepareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });

    }

    public JPanel getPanel() {
        return panel;
    }

    public void saveData(){
        String idValue = idInput.getText();
        if (!checkId()) {
            resetPanel();
            return;
        }

        String emplValue = employeeInput.getText();
        Integer id = Integer.parseInt(idValue);
        Boolean result = Order.carpetsOnTransport(id, emplValue);

        if (result) {
            prepareTicket(id);
        } else {
            showMessageDialog("Greška", "Došlo je do greške prilikom čuvanja izmena. Pokušajte ponovo.");
        }
    }

    public void showMessageDialog(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.PLAIN_MESSAGE);
    }

    public void prepareTicket(Integer id) {
        Order order = Order.getOrder(id);
        if (order != null) {
            fillInPanel(order);
        } else {
            showMessageDialog("Greška", "Došlo je do greške prilikom štampanja priznanice. Pokušajte ponovo.");
        }
    }

    public void printTicketPanel(){
        try {
            Toolkit tkp = panelRight.getToolkit();
            PrintJob pjp = tkp.getPrintJob(MF.getInstance().getFrame(), null, null);
            Graphics g = pjp.getGraphics();
            panelRight.printAll(g);
            g.dispose();
            pjp.end();
            showMessageDialog("Obaveštenje", "Štampanje priznanice završeno.");

            MainFrame.getInstance().emptyPanelInput();
        } catch (Exception pE) {
            pE.printStackTrace();
        }
    }

    public void fillInPanel(Order order) {
        String address = order.getStreet() + " " + order.getStreetNumber() + ", " + order.getTown();
        if (order.getBuilding()) {
            address += " (" + order.getApartmentNumber() + "/" + order.getFloor() + ")";
        }
        String date = "";
        try {
            date = order.getRetrievalDate().toString();
        }catch (Exception e) {}

        labelInput.setText(order.getId().toString());
        nameInput.setText(order.getCustomerName());
        numberPhInput.setText(order.getCustomerPhoneNumber());
        addressInput.setText(address);
        if (order.getQuadrature() > 0) {
            quadratureInput.setText(order.getQuadrature().toString());
        }
        if (order.getPricePerQ() > 0) {
            ppqInput.setText(order.getPricePerQ().toString());
        }
        if (order.getCarpetNumber() > 0) {
            carpetNumInput.setText(order.getCarpetNumber().toString());
        }
        if (order.getDiscount() && order.getDiscountPercentage() > 0) {
            discountInput.setText(order.getDiscountPercentage() + "%");
        }
        if (order.getTransportCost() > 0) {
            transportInput.setText(order.getTransportCost().toString());
        }
        if (order.getPrice() > 0) {
            priceInput.setText(order.getPrice().toString() + " DIN");
        }
        receiptDateInput.setText(order.getReceiptDate().toString());
        receiptEmployeeInput.setText(order.getReceiptEmployee());
        retrievalDateInput.setText(date);
        if (order.getRetrievalEmployee() != null && !order.getRetrievalEmployee().equals("")) {
            retrievalEmployeeInput.setText(order.getRetrievalEmployee());
        }

    }

    public void resetPanel() {
        labelInput.setText("");
        nameInput.setText("");
        numberPhInput.setText("");
        addressInput.setText("");
        quadratureInput.setText("");
        ppqInput.setText("");
        carpetNumInput.setText("");
        discountInput.setText("");
        transportInput.setText("");
        priceInput.setText("");
        receiptDateInput.setText("");
        receiptEmployeeInput.setText("");
        retrievalDateInput.setText("");
        retrievalEmployeeInput.setText("");

    }

    @SuppressWarnings("Duplicates")
    public Boolean checkId() {
        if (!idInput.getText().equals("")) {
            try {
                Integer id = Integer.parseInt(idInput.getText());
                Order order = Order.getOrder(id);
                if (order == null) {
                    showMessageDialog("Oprez", "Ne postoji narudžba sa unesenom šifrom!");
                    idInput.setText("");
                    idInput.requestFocus();
                    return false;
                }
            } catch (Exception e) {
                showMessageDialog("Oprez", "Šifra narudžbe može sadržati samo brojeve!");
                idInput.setText("");
                idInput.requestFocus();
                return false;
            }

        } else {
            showMessageDialog("Oprez", "Šifra narudžbe ne može ostati nepopunjena!");
            idInput.setText("");
            idInput.requestFocus();
            return false;
        }
        return true;
    }

    public void initializeLogo() {
        try {
            imageLabel.setIcon(new ImageIcon(getClass().getResource("/resources/images/logo-resized.png")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
