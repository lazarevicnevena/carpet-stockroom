package view;

import model.Order;

import javax.swing.*;
import java.awt.event.*;

public class StatusPanel {
    private JPanel panel;
    private JTextField idInput;
    private JComboBox newStateInput;
    private JButton cancelButton;
    private JButton saveButton;
    private JLabel currentStatusLabel;

    public StatusPanel() {
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                MainFrame.getInstance().emptyPanelInput();
            }
        });


        idInput.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                currentStatusLabel.setText("/");
            }
        });

        idInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String idValue = idInput.getText();
                if (!idValue.equals("")) {
                    try {
                        Integer id = Integer.parseInt(idValue);
                        String state = Order.getCurrentStatus(id);
                        currentStatusLabel.setText(state);
                    } catch (Exception ex) {
                        showMessageDialog("Oprez", "Šifra narudžbe može sadržati samo brojeve!");
                        idInput.setText("");
                        idInput.requestFocus();
                    }
                }
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }


    public void saveData(){
        String idValue = idInput.getText();
        if (!checkId()) {
            return;
        }

        Integer id = Integer.parseInt(idValue);
        String newState = String.valueOf(newStateInput.getSelectedItem());
        Boolean result = Order.changeStatus(id, newState);
        if (result) {
            showMessageDialog("Obaveštenje", "Status narudžbe uspešno promenjen.");
            MainFrame.getInstance().emptyPanelInput();
        } else {
            showMessageDialog("Greška", "Došlo je do greške prilikom promene statusa. Pokušajte ponovo.");
        }
    }

    public void showMessageDialog(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.PLAIN_MESSAGE);
    }

    @SuppressWarnings("Duplicates")
    public Boolean checkId() {
        if (!idInput.getText().equals("")) {
            try {
                Integer id = Integer.parseInt(idInput.getText());
                Order order = Order.getOrder(id);
                if (order == null) {
                    showMessageDialog("Oprez", "Ne postoji narudžba sa unesenom šifrom!");
                    idInput.setText("");
                    idInput.requestFocus();
                    return false;
                }
            } catch (Exception e) {
                showMessageDialog("Oprez", "Šifra narudžbe može sadržati samo brojeve!");
                idInput.setText("");
                idInput.requestFocus();
                return false;
            }

        } else {
            showMessageDialog("Oprez", "Šifra narudžbe ne može ostati nepopunjena!");
            idInput.setText("");
            idInput.requestFocus();
            return false;
        }
        return true;
    }
}
